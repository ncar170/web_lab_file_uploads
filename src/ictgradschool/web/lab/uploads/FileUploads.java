package ictgradschool.web.lab.uploads;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


public class FileUploads extends HttpServlet {

    private File uploadsFolder;
    private File tempFolder;

    @Override
    public void init() throws ServletException{ //This init() method is run as soon as the servlet is created
        super.init();

        // using getServletContext() with .getRealPath allows us to create a new file within a specific folder regardless of where code is being run from
        this.uploadsFolder = new File(getServletContext().getRealPath("/Uploaded-Photos"));
        if (!uploadsFolder.exists()){
            uploadsFolder.mkdirs();
        }

        this.tempFolder = new File(getServletContext().getRealPath("/WEB-INF/temp"));
        if (!tempFolder.exists()){
            tempFolder.mkdirs();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //DFTI is a class with APIs to help storing files in the temporary folder and with other aspects of the upload process.
        //ServletFileUpload is a class with APIs that help with aspects of the uploading process.
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(4 * 1024);
        factory.setRepository(tempFolder);
        ServletFileUpload upload = new ServletFileUpload(factory);

        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        try {
            List<FileItem> fileItems = upload.parseRequest(req); //creates A File object for File from the form
            //FileItem represents a file or form item that was received within a multipart/form-data post

            File fullSizeImageFile = null;
            //loops through all items that are coming from a multipart/form-data post
            for (FileItem f : fileItems){
                if (!f.isFormField() && (f.getContentType().equals("image/png") || f.getContentType().equals("image/jpeg"))){
                    String fileName = f.getName();
                    fullSizeImageFile = new File(uploadsFolder, fileName);
                    f.write(fullSizeImageFile); //writes the form item to 'fullSizeImageFile'
                }

            }
            //gets the name of the file that was just uploaded to the servlet, and adds it to the src attribute of an img tag

//            out.println("<img src=../Uploaded-Photos/" + fullSizeImageFile.getName() + " width=\"200\">");

        } catch (Exception e){
            throw new ServletException(e);
        }


        //This loops through all images in the folder, and prints out the image to the html page as well as the name of the image
        File[] allImages = uploadsFolder.listFiles();
        for (int i = 0; i < allImages.length; i++){
            out.println("<figure>");
            out.println("<img src=../Uploaded-Photos/" + allImages[i].getName() + " width=\"200\">");
            out.println("<figcaption>" + allImages[i].getName() + "</figcaption>");
            out.println("</figure>");
        }

    }

}
